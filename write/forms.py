from django import forms
from .models import MyModel

class MyForm(forms.ModelForm):
  class Meta:
    model = MyModel
    fields = ["firstname", "lastname", "country", "team", "age",]
    labels = {'firstname': "FirstName", 'lastname': "LastName", 'country': "Country", 'team' : "Team", 'age': "Age",}
