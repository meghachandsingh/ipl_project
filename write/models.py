from django.db import models

class MyModel(models.Model):
    firstname = models.CharField(max_length=200)
    lastname = models.CharField(max_length=200)
    country = models.CharField(max_length=200)
    team = models.CharField(max_length=200)
    age = models.IntegerField()	
