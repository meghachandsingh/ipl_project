from django.contrib import admin
from write.models import MyModel

# Register your models here.

class MemberAdmin(admin.ModelAdmin):
  list_display = ("firstname", "lastname",)

admin.site.register(MyModel, MemberAdmin)
